package smarthome.back;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import smarthome.back.SmartObject.SmartLamps;
import smarthome.back.SmartObject.SmartRadiators;

@SpringBootApplication
public class BackApplication  extends SpringBootServletInitializer {

    public static void main(String[] args) {

      //  SpringApplication.run(BackApplication.class, args);
          new SpringApplicationBuilder(BackApplication.class)
                  .run(args);

        SmartLamps.CreateListLamp();
        SmartRadiators.CreateListRadiator();


    }

/*
    public static void main(String[] args)
    {
        SpringApplication.run(SmartHomeApiApp.class, args);

      //  new SpringApplicationBuilder(SmartHomeApiApp.class)
      //          .web(true)
      //          .run(args);

        SmartLamps.CreateListLamp();
        //SmartRights.GetListRightsFromDAO();
        //SmartUsers.GetListUsersFromDAO();
    }*/

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(BackApplication .class);
    }

}

