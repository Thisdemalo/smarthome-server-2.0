package smarthome.back.Dao;

import smarthome.back.Model.ModelDatabase.RoomType;

import java.sql.ResultSet;
import java.sql.SQLException;

public class RoomTypeDao extends Dao<RoomType> {

    public RoomType findRoomTypeByName(String room) {

        RoomType roomType = new RoomType();
        try {
            ResultSet result = this .connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery("SELECT * FROM room_type WHERE name = '" + room + "'");
            if(result.first())
                roomType = new RoomType(result.getInt("id"),result.getString("name"), result.getString("description"));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return roomType;

    }


    @Override
    public RoomType find(int id) {

        RoomType roomType = new RoomType();
        try {
            ResultSet result = this .connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery("SELECT * FROM room_type WHERE id = " + id);
            if(result.first())
                roomType = new RoomType(id,result.getString("name"), result.getString("description"));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return roomType;

    }

    @Override
    public RoomType create(RoomType obj) {
        return null;
    }

    @Override
    public RoomType update(RoomType obj) {
        return null;
    }

    @Override
    public void delete(RoomType obj) {

    }
}
