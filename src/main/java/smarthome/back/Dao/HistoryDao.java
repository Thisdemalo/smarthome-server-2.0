package smarthome.back.Dao;

import smarthome.back.Model.ModelDatabase.History;

import java.sql.ResultSet;
import java.sql.SQLException;

public class HistoryDao extends Dao<History>{
    @Override
    public History find(int id) {

        History history = new History();
        try {
            ResultSet result = this .connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery("SELECT * FROM history WHERE id = " + id);
            if(result.first())
                history = new History(id, result.getInt("element"),result.getInt("action"), result.getDate("start_date"), result.getString("position"), result.getDate("end_date"));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return history;
    }

    @Override
    public History create(History obj) {
        return null;
    }

    @Override
    public History update(History obj) {
        return null;
    }

    @Override
    public void delete(History obj) {

    }
}
