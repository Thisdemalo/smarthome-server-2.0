package smarthome.back.Dao;

import smarthome.back.Model.ModelFront.Lamp;
import smarthome.back.Model.ModelFront.Radiator;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class FrontReadyDao extends Dao {

    private List<Lamp> getAllObjectsByTypeByUser(String login, String action, String element){

        String loginSql = "WHERE u.login = '" + login  +"' AND ";

        if(login == null){
            loginSql = "WHERE u.id >= 0 AND ";
        }


        List<Lamp> lampList = new ArrayList<Lamp>();
        try {

            String sql = "SELECT e.id as 'id', e.name as 'name', rt.name as 'piece', h.position as '"+ action +"' " +
                    "FROM users u " +
                    "LEFT JOIN right_element r ON u.id = r.user " +
                    "LEFT JOIN elements e ON e.id = r.element " +
                    "LEFT JOIN room_type rt ON rt.id = e.room " +
                    "LEFT JOIN object_type o ON o.id = e.object_type " +
                    "LEFT JOIN history h ON h.element = e.id " +
                    "LEFT JOIN action_type a ON a.id = h.action " +
                    loginSql +
                    "o.name = '" + element +"' AND " +
                    "a.name = '" + action + "' " +
                    "GROUP BY e.id " +
                    "ORDER BY h.id DESC";

            System.out.println(sql);

            ResultSet result = this.connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery(sql);
            while(result.next()){

                int intensity = -1;
                String color = null;

                if(action == "Intensity"){
                    intensity = result.getInt("intensity");
                }

                if(action == "Color"){
                    color = result.getString("color");
                }

                Lamp lamp = new Lamp(
                        result.getInt("id"),
                        result.getString("name"),
                        result.getString("piece"),
                        intensity,
                        color
                );

                lampList.add(lamp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return lampList;
    }

    public List<Lamp> getAllLampByUser(String login){

        List<Lamp> intensityList = this.getAllObjectsByTypeByUser(login, "Intensity","Lamp");
        List<Lamp> colorList = this.getAllObjectsByTypeByUser(login, "Color","Lamp");
        List<Lamp> generalList = new ArrayList<Lamp>();

        if(colorList.isEmpty()){
            if(intensityList.isEmpty()){
                return null;
            } else {
                return intensityList;
            }
        }

        if(intensityList.isEmpty()){
            return colorList;
        }

        for (Lamp l_intensity: intensityList) {
            for(Lamp l_color:colorList){
                if(l_intensity.getId() == l_color.getId()){
                    l_intensity.setColor(l_color.getColor());
                }
                generalList.add(l_intensity);
            }
        }

        return generalList;

    }

    public List<Lamp> getAllLamps(){

        List<Lamp> lampList = new ArrayList<Lamp>();

        try {

            String sqlQuery =
                    " SELECT e.id as 'id', " +
                            " e.name as 'name', " +
                            " rt.name as 'piece', " +
                            " (SELECT h.position FROM history h, action_type a, object_type o WHERE a.name='Intensity' AND a.id = h.action AND e.id=h.element AND o.name='Lamp' ORDER BY h.id DESC LIMIT 1) as 'intensity', " +
                            " (SELECT h.position FROM history h, action_type a, object_type o WHERE a.name='Color' AND a.id = h.action AND e.id=h.element AND o.name='Lamp' ORDER BY h.id DESC LIMIT 1) as 'color' " +
                            " FROM elements e, room_type rt " +
                            " WHERE e.room = rt.id AND e.object_type=1" +
                            " GROUP BY e.id";

            ResultSet result = this.connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery(sqlQuery);
            while(result.next()){


                Lamp lamp = new Lamp(
                        result.getInt("id"),
                        result.getString("name"),
                        result.getString("piece"),
                        result.getInt("intensity"),
                        result.getString("color")
                );

                lampList.add(lamp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return lampList;
    }

    public List<Radiator> getAllRadiators(){

        List<Radiator> radiatorList = new ArrayList<Radiator>();

        try {

            String sqlQuery =
                    " SELECT e.id as 'id', " +
                            " e.name as 'name', " +
                            " rt.name as 'piece', " +
                            " (SELECT h.position FROM history h, action_type a, object_type o WHERE a.name='Intensity' AND a.id = h.action AND e.id=h.element AND o.name='Radiator' ORDER BY h.id DESC LIMIT 1) as 'intensity' " +
                            " FROM elements e, room_type rt " +
                            " WHERE e.room = rt.id AND e.object_type=3" +
                            " GROUP BY e.id";

            ResultSet result = this.connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery(sqlQuery);
            while(result.next()){


                Radiator radiator = new Radiator(
                        result.getInt("id"),
                        result.getString("name"),
                        result.getString("piece"),
                        result.getInt("intensity")
                );

                radiatorList.add(radiator);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return radiatorList;
    }


    @Override
    public Object find(int id) {
        return null;
    }

    @Override
    public Object create(Object obj) {
        return null;
    }

    @Override
    public Object update(Object obj) {
        return null;
    }

    @Override
    public void delete(Object obj) {

    }
}
