package smarthome.back.Dao;

import smarthome.back.Model.ModelDatabase.RightElement;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class RightElementDao extends Dao {

    public List<RightElement> getAllRight(){

        List<RightElement> rightElements = new ArrayList<RightElement>();
        try {
            ResultSet result = this .connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery("SELECT * FROM right_element");
            while(result.next()){
                RightElement rightElement = new RightElement(result.getInt("id"), result.getInt("user"),result.getInt("element"), result.getString("right_type"));
                rightElements.add(rightElement);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return rightElements;
    }

    public List<RightElement> getAllRightByUser(int id){

        List<RightElement> rightElements = new ArrayList<RightElement>();
        try {
            ResultSet result = this .connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery("SELECT * FROM right_element WHERE user = " + id);
            while(result.next()){
                RightElement rightElement = new RightElement(result.getInt("id"), result.getInt("user"),result.getInt("element"), result.getString("right_type"));
                rightElements.add(rightElement);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return rightElements;
    }

    @Override
    public Object find(int id) {
        return null;
    }

    @Override
    public Object create(Object obj) {
        return null;
    }

    @Override
    public Object update(Object obj) {
        return null;
    }

    @Override
    public void delete(Object obj) {

    }
}
