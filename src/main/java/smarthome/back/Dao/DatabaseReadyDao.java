package smarthome.back.Dao;

import smarthome.back.Appliances.Lamp.Yeelight;
import smarthome.back.Model.ModelDatabase.*;
import smarthome.back.Model.ModelFront.Lamp;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

public class DatabaseReadyDao extends Dao {

    public void insertLampInDb(Lamp lamp){

        ObjectTypeDao objectTypeDao = new ObjectTypeDao();
        ObjectType o = objectTypeDao.findObjectTypeByName("Lamp");

        RoomTypeDao roomTypeDao = new RoomTypeDao();
        RoomType r = roomTypeDao.findRoomTypeByName(lamp.getPiece());

        String name = lamp.getName();
        int objectType = o.getId();
        int room = r.getId();
        String description = "User lamp";
        int consumtionHours = -1;

        try {
            Statement statement = this.connect.createStatement();
            statement.executeUpdate("INSERT INTO elements(name, object_type, room, description, consumption_hours) " +
                    "VALUE ('"+ name+"', "+ objectType +", "+  room +", '"+ description +"', "+consumtionHours+" ) ");
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }


    public void updateLampInDb(Lamp lamp){

        ElementsDao elementsDao = new ElementsDao();
        Elements elements = elementsDao.find(lamp.getId());

        ObjectTypeDao objectTypeDao = new ObjectTypeDao();
        ObjectType o = objectTypeDao.findObjectTypeByName("Lamp");

        RoomTypeDao roomTypeDao = new RoomTypeDao();
        RoomType r = roomTypeDao.findRoomTypeByName(lamp.getPiece());

        ActionTypeDao actionTypeDao = new ActionTypeDao();
        ActionType colorAction = actionTypeDao.findActionByName("Color");
        ActionType intensityAction = actionTypeDao.findActionByName("Intensity");

        String name = lamp.getName();
        int objectType = o.getId();
        int room = r.getId();
        String description = "User lamp";
        int consumtionHours = -1;


        try {

            String sqlQueryColor = " SELECT * FROM smarthome.history h WHERE h.action = "+colorAction.getId()+" AND h.element = " + elements.getId() +" ORDER BY h.id DESC LIMIT 1";
            String sqlQueryIntensity = " SELECT * FROM smarthome.history h WHERE h.action = "+intensityAction.getId()+" AND h.element = " + elements.getId() +" ORDER BY h.id DESC LIMIT 1";

            History historyColor = new History();
            History historyIntensity = new History();

            ResultSet resultColor = this.connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery(sqlQueryColor);
            if(resultColor.first()){
                historyColor = new History(
                        resultColor.getInt("id"),
                        resultColor.getInt("element"),
                        resultColor.getInt("action"),
                        resultColor.getDate("start_date"),
                        resultColor.getString("position"),
                        resultColor.getDate("end_date")
                );
            }

            ResultSet resultIntensity = this.connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery(sqlQueryIntensity);
            if(resultIntensity.first()){

                historyIntensity = new History(
                        resultIntensity.getInt("id"),
                        resultIntensity.getInt("element"),
                        resultIntensity.getInt("action"),
                        resultIntensity.getDate("start_date"),
                        resultIntensity.getString("position"),
                        resultIntensity.getDate("end_date")
                        );
            }

            Statement statement = this.connect.createStatement();

            /*
             * Mise à jour et insertion pour un changement de la couleur
             */
            if(!historyColor.getPosition().equals(lamp.getColor())){
                statement.executeUpdate("INSERT INTO history(element, action, start_date, position) VALUE ("+ elements.getId()+", "+ colorAction.getId() +", NOW(), '"+ lamp.getColor() +"') ");
                statement.executeUpdate("UPDATE history SET end_date = NOW() WHERE id = " + historyColor.getId());
                System.out.println("HistoryColor");
            }

            /*
             * Mise à jour et insertion pour un changement de intensity
             */
            if(Integer.parseInt(historyIntensity.getPosition()) != lamp.getIntensity()){
                statement.executeUpdate("INSERT INTO history(element, action, start_date, position) VALUE ("+ elements.getId()+", "+ intensityAction.getId() +", NOW(), '"+ lamp.getIntensity() +"' ) ");
                statement.executeUpdate("UPDATE history SET end_date = NOW() WHERE id = "+ historyIntensity.getId());
                System.out.println("HistoryIntensity");
/*
                try{
                    String s = Yeelight.setBright(lamp.getIntensity()+"");
                    System.out.println(" s = " + s);
                } catch (Exception e){
                    System.out.println(e);
                }*/
            }

            /*
             * Mise à jour du nom du nom de la lampe
             */
            statement.executeUpdate("UPDATE elements SET name = '" + lamp.getName() +"' WHERE id = "+lamp.getId());

        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

    @Override
    public Object find(int id) {
        return null;
    }

    @Override
    public Object create(Object obj) {
        return null;
    }

    @Override
    public Object update(Object obj) {
        return null;
    }

    @Override
    public void delete(Object obj) {

    }
}
