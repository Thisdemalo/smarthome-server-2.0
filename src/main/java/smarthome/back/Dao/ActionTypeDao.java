package smarthome.back.Dao;

import smarthome.back.Model.ModelDatabase.ActionType;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ActionTypeDao extends Dao<ActionType> {

    public ActionType findActionByName(String name){
        ActionType actionType = new ActionType();
        try {
            ResultSet result = this .connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery("SELECT * FROM action_type WHERE name = '" + name + "'");
            if(result.first())
                actionType = new ActionType(result.getInt("id"), result.getString("name"), result.getString("description"));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return actionType;
    }

    @Override
    public ActionType find(int id) {
        ActionType actionType = new ActionType();
        try {
            ResultSet result = this .connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery("SELECT * FROM action_type WHERE id = " + id);
            if(result.first())
                actionType = new ActionType(id, result.getString("name"), result.getString("description"));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return actionType;
    }

    @Override
    public ActionType create(ActionType obj) {
        return null;
    }

    @Override
    public ActionType update(ActionType obj) {
        return null;
    }

    @Override
    public void delete(ActionType obj) {

    }
}
