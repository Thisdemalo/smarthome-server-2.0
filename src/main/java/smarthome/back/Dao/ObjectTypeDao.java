package smarthome.back.Dao;

import smarthome.back.Model.ModelDatabase.ObjectType;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ObjectTypeDao extends Dao<ObjectType> {

    public ObjectType findObjectTypeByName(String name) {
        ObjectType ob = new ObjectType();
        try {
            ResultSet result = this .connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery("SELECT * FROM object_type WHERE name = '" + name +"'");
            if(result.first())
                ob = new ObjectType(result.getInt("id"),result.getString("name"), result.getString("description"));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return ob;
    }


    @Override
    public ObjectType find(int id) {
        ObjectType ob = new ObjectType();
        try {
            ResultSet result = this .connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery("SELECT * FROM object_type WHERE id = " + id);
            if(result.first())
                ob = new ObjectType(id,result.getString("name"), result.getString("description"));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return ob;
    }

    @Override
    public ObjectType create(ObjectType obj) {
        return null;
    }

    @Override
    public ObjectType update(ObjectType obj) {
        return null;
    }

    @Override
    public void delete(ObjectType obj) {

    }
}
