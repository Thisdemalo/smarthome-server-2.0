package smarthome.back.Dao;

import smarthome.back.Model.ModelStatistics.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class StatistiqueDao extends Dao  {

    public List<ElementConsumption> getElementConsumptionLastHours(){

        List<ElementConsumption> listObjectStatistic = new ArrayList<>();
        try {
            ResultSet result = this .connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery("SELECT * FROM smarthome.Element_Consumption_Hours "+
" WHERE (`year` = year(adddate(now(),interval -24 hour)) AND `month` = month(adddate(now(),interval -24 hour)) AND `day` = dayofmonth(adddate(now(),interval -24 hour))AND `hour` >= hour(adddate(now(),interval -24 hour))) " +
" OR (if(day(adddate(now(), interval - 24 hour))-day(now())=0,'',`year` >= year(now()) AND `month` >= month(now()) and `day`>=dayofmonth(now()) )) "+
" ORDER BY `year`, `month`,`day`, `hour`;");
            while (result.next()){

                ElementConsumption elementConsumption = new ElementConsumption(
                        result.getInt("hour"),
                        result.getInt("day"),
                        result.getInt("month"),
                        result.getInt("year"),
                        result.getString("name_element"),
                        result.getString("room_element"),
                        result.getInt("id_element"),
                        result.getFloat("duration_hour"),
                        result.getFloat("consumption_watt_hours"));
                listObjectStatistic.add(elementConsumption);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listObjectStatistic;
    }


    public List<ElementConsumption> getElementConsumptionDay(){

        List<ElementConsumption> listObjectStatistic = new ArrayList<>();
        try {
            ResultSet result = this .connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery("SELECT * FROM smarthome.Element_Consumption_Day " +
                    "WHERE (`year` = year(adddate(now(),interval -7 day)) AND `month` = month(adddate(now(),interval -7 day)) AND `day` >= dayofmonth(adddate(now(),interval -7 day))) " +
                    "OR (if(month(adddate(now(), interval -7 day))-month(now())=0,'',`year` >= year(now()) AND `month` >= month(now()))) " +
                    "ORDER BY `year`, `month`,`day`;");
            while (result.next()){

                ElementConsumption elementConsumption = new ElementConsumption(
                        result.getInt("day"),
                        result.getInt("month"),
                        result.getInt("year"),
                        result.getString("name_element"),
                        result.getString("room_element"),
                        result.getInt("id_element"),
                        result.getFloat("duration_hour"),
                        result.getFloat("consumption_watt_hours"));
                listObjectStatistic.add(elementConsumption);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listObjectStatistic;
    }


    public List<ElementConsumption> getElementConsumptionMonth(){

        List<ElementConsumption> listObjectStatistic = new ArrayList<>();
        try {
            ResultSet result = this .connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery("SELECT * FROM smarthome.Element_Consumption_Day " +
                    "WHERE (`year` = year(adddate(now(),interval -1 month)) AND `month` = month(adddate(now(),interval -1 month)) AND `day` >= dayofmonth(adddate(now(),interval -1 month))) " +
                    "OR (if(month(adddate(now(), interval -1 month))-month(now())=0,'',`year` >= year(now()) AND `month` >= month(now()))) " +
                    "ORDER BY `year`, `month`,`day`;");
            while (result.next()){

                ElementConsumption elementConsumption = new ElementConsumption(
                        result.getInt("day"),
                        result.getInt("month"),
                        result.getInt("year"),
                        result.getString("name_element"),
                        result.getString("room_element"),
                        result.getInt("id_element"),
                        result.getFloat("duration_hour"),
                        result.getFloat("consumption_watt_hours"));

                listObjectStatistic.add(elementConsumption);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listObjectStatistic;
    }


    public List<ElementConsumption> getElementConsumptionYear(){

        List<ElementConsumption> listObjectStatistic = new ArrayList<>();
        try {
            ResultSet result = this .connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery("SELECT * FROM smarthome.Element_Consumption_Month " +
                    "WHERE (`year` = year(adddate(now(),interval -1 year)) AND `month` = month(adddate(now(),interval -1 year))) " +
                    "OR (if(year(adddate(now(), interval -1 year))-year(now())=0,'',`year` = year(now()))) " +
                    "ORDER BY `year`, `month`;");
            while (result.next()){

                ElementConsumption elementConsumption = new ElementConsumption(
                        result.getInt("month"),
                        result.getInt("year"),
                        result.getString("name_element"),
                        result.getString("room_element"),
                        result.getInt("id_element"),
                        result.getFloat("duration_hour"),
                        result.getFloat("consumption_watt_hours"));
                listObjectStatistic.add(elementConsumption);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listObjectStatistic;
    }

    public List<GlobalConsumption> getGlobalConsumptionHours(){

        List<GlobalConsumption> listObjectStatistic = new ArrayList<>();
        try {
            ResultSet result = this .connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery("SELECT * FROM Global_Consumption_Hours");
            while (result.next()){

                GlobalConsumption elementConsumption = new GlobalConsumption(
                        result.getInt("hour"),
                        result.getInt("day"),
                        result.getInt("month"),
                        result.getInt("year"),
                        result.getFloat("consumption_watt_hours"));
                listObjectStatistic.add(elementConsumption);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listObjectStatistic;
    }

    public List<GlobalConsumption> getGlobalConsumptionDay(){

        List<GlobalConsumption> listObjectStatistic = new ArrayList<>();
        try {
            ResultSet result = this .connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery("SELECT * FROM Global_Consumption_Day");
            while (result.next()){

                GlobalConsumption elementConsumption = new GlobalConsumption(
                        result.getInt("day"),
                        result.getInt("month"),
                        result.getInt("year"),
                        result.getFloat("consumption_watt_hours"));
                listObjectStatistic.add(elementConsumption);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listObjectStatistic;
    }

    public List<GlobalConsumption> getGlobalConsumptionMonth(){

        List<GlobalConsumption> listObjectStatistic = new ArrayList<>();
        try {
            ResultSet result = this .connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery("SELECT * FROM Global_Consumption_Month");
            while (result.next()){

                GlobalConsumption elementConsumption = new GlobalConsumption(
                        result.getInt("month"),
                        result.getInt("year"),
                        result.getFloat("consumption_watt_hours"));
                listObjectStatistic.add(elementConsumption);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listObjectStatistic;
    }

    public List<GlobalConsumption> getGlobalConsumptionYear(){

        List<GlobalConsumption> listObjectStatistic = new ArrayList<>();
        try {
            ResultSet result = this .connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery("SELECT * FROM Global_Consumption_Year");
            while (result.next()){

                GlobalConsumption elementConsumption = new GlobalConsumption(
                        result.getInt("year"),
                        result.getFloat("consumption_watt_hours"));
                listObjectStatistic.add(elementConsumption);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listObjectStatistic;
    }

    public List<ObjectConsumption> getObjectConsumptionHours(){

        List<ObjectConsumption> listObjectStatistic = new ArrayList<>();
        try {
            ResultSet result = this .connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery("SELECT * FROM Object_Consumption_Hours");
            while (result.next()){

                ObjectConsumption elementConsumption = new ObjectConsumption(
                        result.getInt("hour"),
                        result.getInt("day"),
                        result.getInt("month"),
                        result.getInt("year"),
                        result.getString("name_object"),
                        result.getInt("id_object"),
                        result.getFloat("duration_hour"),
                        result.getFloat("consumption_watt_hours"));
                listObjectStatistic.add(elementConsumption);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listObjectStatistic;
    }

    public List<ObjectConsumption> getObjectConsumptionDay(){

        List<ObjectConsumption> listObjectStatistic = new ArrayList<>();
        try {
            ResultSet result = this .connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery("SELECT * FROM Object_Consumption_Day");
            while (result.next()){

                ObjectConsumption elementConsumption = new ObjectConsumption(
                        result.getInt("day"),
                        result.getInt("month"),
                        result.getInt("year"),
                        result.getString("name_object"),
                        result.getInt("id_object"),
                        result.getFloat("duration_hour"),
                        result.getFloat("consumption_watt_hours"));
                listObjectStatistic.add(elementConsumption);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listObjectStatistic;
    }


    public List<ObjectConsumption> getObjectConsumptionMonth(){

        List<ObjectConsumption> listObjectStatistic = new ArrayList<>();
        try {
            ResultSet result = this .connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery("SELECT * FROM Object_Consumption_Month");
            while (result.next()){

                ObjectConsumption elementConsumption = new ObjectConsumption(
                        result.getInt("month"),
                        result.getInt("year"),
                        result.getString("name_object"),
                        result.getInt("id_object"),
                        result.getFloat("duration_hour"),
                        result.getFloat("consumption_watt_hours"));
                listObjectStatistic.add(elementConsumption);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listObjectStatistic;
    }


    public List<ObjectConsumption> getObjectConsumptionYear(){

        List<ObjectConsumption> listObjectStatistic = new ArrayList<>();
        try {
            ResultSet result = this .connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery("SELECT * FROM Object_Consumption_Year");
            while (result.next()){

                ObjectConsumption elementConsumption = new ObjectConsumption(
                        result.getInt("year"),
                        result.getString("name_object"),
                        result.getInt("id_object"),
                        result.getFloat("duration_hour"),
                        result.getFloat("consumption_watt_hours"));
                listObjectStatistic.add(elementConsumption);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listObjectStatistic;
    }


    public List<RoomConsumption> getRoomConsumptionHours(){

        List<RoomConsumption> listObjectStatistic = new ArrayList<>();
        try {
            ResultSet result = this .connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery("SELECT * FROM Room_Consumption_Hours");
            while (result.next()){

                RoomConsumption elementConsumption = new RoomConsumption(
                        result.getInt("hour"),
                        result.getInt("day"),
                        result.getInt("month"),
                        result.getInt("year"),
                        result.getString("name_room"),
                        result.getInt("id_room"),
                        result.getFloat("duration_hour"),
                        result.getFloat("consumption_watt_hours"));
                listObjectStatistic.add(elementConsumption);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listObjectStatistic;
    }



    public List<RoomConsumption> getRoomConsumptionDay(){

        List<RoomConsumption> listObjectStatistic = new ArrayList<>();
        try {
            ResultSet result = this .connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery("SELECT * FROM Room_Consumption_Day");
            while (result.next()){

                RoomConsumption elementConsumption = new RoomConsumption(
                        result.getInt("day"),
                        result.getInt("month"),
                        result.getInt("year"),
                        result.getString("name_room"),
                        result.getInt("id_room"),
                        result.getFloat("duration_hour"),
                        result.getFloat("consumption_watt_hours"));
                listObjectStatistic.add(elementConsumption);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listObjectStatistic;
    }

    public List<RoomConsumption> getRoomConsumptionMonth(){

        List<RoomConsumption> listObjectStatistic = new ArrayList<>();
        try {
            ResultSet result = this .connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery("SELECT * FROM Room_Consumption_Month");
            while (result.next()){

                RoomConsumption elementConsumption = new RoomConsumption(
                        result.getInt("month"),
                        result.getInt("year"),
                        result.getString("name_room"),
                        result.getInt("id_room"),
                        result.getFloat("duration_hour"),
                        result.getFloat("consumption_watt_hours"));
                listObjectStatistic.add(elementConsumption);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listObjectStatistic;
    }


    public List<RoomConsumption> getRoomConsumptionYear(){

        List<RoomConsumption> listObjectStatistic = new ArrayList<>();
        try {
            ResultSet result = this .connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery("SELECT * FROM Room_Consumption_Year");
            while (result.next()){

                RoomConsumption elementConsumption = new RoomConsumption(
                        result.getInt("year"),
                        result.getString("name_room"),
                        result.getInt("id_room"),
                        result.getFloat("duration_hour"),
                        result.getFloat("consumption_watt_hours"));
                listObjectStatistic.add(elementConsumption);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listObjectStatistic;
    }

    @Override
    public Object find(int id) {
        return null;
    }

    @Override
    public Object create(Object obj) {
        return null;
    }

    @Override
    public Object update(Object obj) {
        return null;
    }

    @Override
    public void delete(Object obj) {

    }
}
