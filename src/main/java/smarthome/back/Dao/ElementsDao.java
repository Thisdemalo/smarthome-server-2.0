package smarthome.back.Dao;

import smarthome.back.Model.ModelDatabase.Elements;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ElementsDao extends Dao<Elements> {


    @Override
    public Elements find(int id) {

        Elements elements = new Elements();
        try {
            ResultSet result = this .connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery("SELECT * FROM elements WHERE id = " + id);
            if(result.first())
                elements = new Elements(id, result.getString("name"), result.getInt("object_type"),result.getInt("room"), result.getString("description"), result.getInt("consumption_hours"));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return elements;
    }

    public Elements findByObjectType(String objectTypeName){
        Elements elements = new Elements();
        try {
            ResultSet result = this .connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery("SELECT * FROM elements INNER JOIN object_type ON elements.object_type = object_type.id WHERE object_type.name =" + objectTypeName);
            if(result.first())
                elements = new Elements(result.getInt("id"), result.getString("name"), result.getInt("object_type"),result.getInt("room"), result.getString("description"), result.getInt("consumption_hours"));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return elements;
    }

    @Override
    public Elements create(Elements obj) {
        return null;
    }

    @Override
    public Elements update(Elements obj) {
        return null;
    }

    @Override
    public void delete(Elements obj) {

    }
}
