package smarthome.back.Dao;

import smarthome.back.Model.ModelDatabase.User;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDao extends Dao{

    public List<User> getAllUser() {

        List<User> listUser = new ArrayList<User>();
        try {
            ResultSet result = this .connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery("SELECT * FROM users");
            while(result.next()){
                User user = new User(result.getInt("id"), result.getString("login"),result.getString("password"), result.getString("role"));
                listUser.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return listUser;
    }

    public User getUserByLogin(String login){

        User user = new User();
        try {
            ResultSet result = this .connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE).executeQuery("SELECT * FROM users WHERE login = '" + login +"'");
            if(result.first())
                user = new User(result.getInt("id"),result.getString("login"), result.getString("password"),result.getString("role"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

    @Override
    public Object find(int id) {
        return null;
    }

    @Override
    public Object create(Object obj) {
        return null;
    }

    @Override
    public Object update(Object obj) {
        return null;
    }

    @Override
    public void delete(Object obj) {

    }
}
