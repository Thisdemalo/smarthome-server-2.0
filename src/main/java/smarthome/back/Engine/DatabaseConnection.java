package smarthome.back.Engine;

import smarthome.back.BackApplication;

import java.sql.Connection;
import java.io.IOException;
import java.sql.*;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;


public class DatabaseConnection {

    /**
     * Hôte de connection
     */
    private static String dbhost;
    /**
     * Base de données de connexion
     */
    private static String database;
    /**
     * Nom du user
     */
    private static String dbport;
    /**
     * Nom du user
     */
    private static String dbuser;
    /**
     * Mot de passe du user
     */
    private static String dbpasswd;
    /**
     * URL de connexion
     */
    private static String dburl;
    /**
     * Objet Connection
     */
    private static Connection connect;

    /**
     * Méthode qui va nous retourner notre instance
     * et la créer si elle n'existe pas...
     * @return
     */

    public static Connection getInstance(){


        Properties prop = new Properties();
        InputStream input = null;


        try {
            //input =  new FileInputStream("src\\main\\config.properties");
            ClassLoader classloader = Thread.currentThread().getContextClassLoader();
            input = classloader.getResourceAsStream("config.properties");
            //input = new FileInputStream("resources/config.properties");

            if(prop == null){
                prop = new Properties();
            }
            //prop.load(SmartHomeApiApp.class.getResourceAsStream("/src/main/config.properties"));
            //prop.load(SmartHomeApiApp.class.getResourceAsStream("\\src\\main\\config.properties"));
            prop.load(input);

            dbhost = prop.getProperty("dbhost");
            database = prop.getProperty("database");
            dbuser = prop.getProperty("dbuser");
            dbpasswd = prop.getProperty("dbpassword");
            dbport = prop.getProperty("dbport");

            dburl = "jdbc:mysql://" + dbhost + ":" + dbport + "/" + database + "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";

        } catch (IOException e) {
            e.printStackTrace();
        }


        if(connect == null){
            try {
                Class.forName("com.mysql.jdbc.Driver");
                connect = DriverManager.getConnection(dburl, dbuser, dbpasswd);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return connect;
    }

}
