package smarthome.back.Controller;

import smarthome.back.Dao.StatistiqueDao;
import smarthome.back.Model.ModelStatistics.*;

import java.util.ArrayList;
import java.util.List;

public class StatistiqueController {

    public static List<ElementConsumption> getElementConsumption(String period){
        List<ElementConsumption> listObjectStatistic = new ArrayList<>();
        StatistiqueDao statistiqueDao = new StatistiqueDao();
        switch (period) {
            case "LastHours":
                listObjectStatistic = statistiqueDao.getElementConsumptionLastHours();
                break;
            case "Week":
                listObjectStatistic = statistiqueDao.getElementConsumptionDay();
                break;
            case "Month":
                listObjectStatistic = statistiqueDao.getElementConsumptionMonth();
                break;
            case "Year":
                listObjectStatistic = statistiqueDao.getElementConsumptionYear();
                break;
        }
        return listObjectStatistic;
    }

    public static List<GlobalConsumption> getGlobalConsumption(String period){
        List<GlobalConsumption> listObjectStatistic = new ArrayList<>();
        StatistiqueDao statistiqueDao = new StatistiqueDao();
        switch (period) {
            case "Hours":
                listObjectStatistic = statistiqueDao.getGlobalConsumptionHours();
                break;
            case "Day":
                listObjectStatistic = statistiqueDao.getGlobalConsumptionDay();
                break;
            case "Month":
                listObjectStatistic = statistiqueDao.getGlobalConsumptionMonth();
                break;
            case "Year":
                listObjectStatistic = statistiqueDao.getGlobalConsumptionYear();
                break;
        }
        return listObjectStatistic;
    }

    public static List<ObjectConsumption> getObjectConsumption(String period){
        List<ObjectConsumption> listObjectStatistic = new ArrayList<>();
        StatistiqueDao statistiqueDao = new StatistiqueDao();
        switch (period) {
            case "Hours":
                listObjectStatistic = statistiqueDao.getObjectConsumptionHours();
                break;
            case "Day":
                listObjectStatistic = statistiqueDao.getObjectConsumptionDay();
                break;
            case "Month":
                listObjectStatistic = statistiqueDao.getObjectConsumptionMonth();
                break;
            case "Year":
                listObjectStatistic = statistiqueDao.getObjectConsumptionYear();
                break;
        }
        return listObjectStatistic;
    }

    public static List<RoomConsumption> getRoomConsumption(String period){
        List<RoomConsumption> listObjectStatistic = new ArrayList<>();
        StatistiqueDao statistiqueDao = new StatistiqueDao();
        switch (period) {
            case "Hours":
                listObjectStatistic = statistiqueDao.getRoomConsumptionHours();
                break;
            case "Day":
                listObjectStatistic = statistiqueDao.getRoomConsumptionDay();
                break;
            case "Month":
                listObjectStatistic = statistiqueDao.getRoomConsumptionMonth();
                break;
            case "Year":
                listObjectStatistic = statistiqueDao.getRoomConsumptionYear();
                break;
        }
        return listObjectStatistic;
    }
}
