package smarthome.back.Controller;


import org.springframework.stereotype.Controller;
import smarthome.back.Dao.DatabaseReadyDao;
import smarthome.back.Model.ModelFront.Lamp;
import smarthome.back.Model.ModelFront.Radiator;
import smarthome.back.SmartObject.SmartLamps;
import smarthome.back.SmartObject.SmartRadiators;

import java.util.List;

@Controller
public class RadiatorController {

    public static List<Radiator> GetListRadiators() {

        if (SmartRadiators.ListRadiators !=null && !SmartRadiators.ListRadiators .isEmpty() ){
            return SmartRadiators.ListRadiators ;
        }
        return null;
    }

    public static List<Radiator> GetListRadiatorsByUser(String login){

        return SmartRadiators.GetAllRadiatorsByUser(login);


    }

    /* public static Lamp SetLampByID(Lamp newLamp)
    {
        DatabaseReadyDao databaseReadyDao = new DatabaseReadyDao();
        try{
            for (Lamp lamp: SmartLamps.ListLamps) {
                if (lamp.getId() == newLamp.getId()){
                    lamp.setName(newLamp.getName());
                    lamp.setColor(newLamp.getColor());
                    lamp.setIntensity(newLamp.getIntensity());
                    lamp.setPiece(newLamp.getPiece());

                    //Appel méthode update base de donnée
                    databaseReadyDao.updateLampInDb(lamp);

                    return lamp;
                }
            }
   }
        catch (Exception e){

        }
        return null;
    }*/

    /*public static List<Lamp> SetAllLamps(List<Lamp> newLampList){
        List<Lamp> lampListReturn = null;

        for (Lamp lamp: newLampList) {
            lampListReturn.add(SetLampByID(lamp));
        }
        return lampListReturn;
    }*/
}
