package smarthome.back.Controller;


import org.springframework.stereotype.Controller;
import smarthome.back.Dao.DatabaseReadyDao;
import smarthome.back.Model.ModelFront.Lamp;
import smarthome.back.SmartObject.SmartLamps;

import java.util.*;

@Controller
public class LampController {

    public static List<Lamp> GetListLamps() {

        if (SmartLamps.ListLamps!=null && !SmartLamps.ListLamps.isEmpty() ){
            return SmartLamps.ListLamps;
        }
        return null;
    }

    public static List<Lamp> GetListLampsByUser(String login){

        return SmartLamps.GetAllLampByUser(login);


    }

    public static Lamp SetLampByID(Lamp newLamp)
    {
        DatabaseReadyDao databaseReadyDao = new DatabaseReadyDao();
        try{
            for (Lamp lamp: SmartLamps.ListLamps) {
                if (lamp.getId() == newLamp.getId()){
                    lamp.setName(newLamp.getName());
                    lamp.setColor(newLamp.getColor());
                    lamp.setIntensity(newLamp.getIntensity());
                    lamp.setPiece(newLamp.getPiece());

                    //Appel méthode update base de donnée
                    databaseReadyDao.updateLampInDb(lamp);

                    return lamp;
                }
            }
   }
        catch (Exception e){

        }
        return null;
    }

    public static List<Lamp> SetAllLamps(List<Lamp> newLampList){
        List<Lamp> lampListReturn = null;

        for (Lamp lamp: newLampList) {
            lampListReturn.add(SetLampByID(lamp));
        }
        return lampListReturn;
    }
}
