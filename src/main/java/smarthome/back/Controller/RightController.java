package smarthome.back.Controller;

import smarthome.back.Dao.RightElementDao;
import smarthome.back.Model.ModelDatabase.RightElement;

import java.util.List;

public class RightController {

    public static List<RightElement> listRightElement;

    public static void setListRightElement(){
        RightElementDao rightElementDao = new RightElementDao();
        listRightElement = rightElementDao.getAllRight();
    }
}
