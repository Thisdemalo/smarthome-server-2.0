package smarthome.back.Controller;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller
public class WebSocketController {

    @MessageMapping("/update")
    @SendTo("/objects/update")
    public boolean update() throws Exception {
        return true;
        //TODO : gérer exception
    }
}
