package smarthome.springbootstarter;

import smarthome.back.Model.ModelDatabase.User;
import smarthome.back.Model.ModelFront.UserFront;

public class Utilities {

    public UserFront convertUserToUserFront(User userBack){
        UserFront userFront = new UserFront();
        userFront.setLogin(userBack.getLogin());
        userFront.setPassword(userBack.getPassword());
        userFront.setId(""+userBack.getId());
        return userFront;
    }

    public User convertUserFrontToUser(UserFront userFront){
        User userBack = new User();
        userBack.setLogin(userFront.getLogin());
        userBack.setPassword(userFront.getPassword());
        userBack.setId(Integer.parseInt(userFront.getId()));

        return userBack;
    }


}
