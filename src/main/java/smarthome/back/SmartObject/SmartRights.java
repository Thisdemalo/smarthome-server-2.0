package smarthome.back.SmartObject;

import smarthome.back.Dao.RightElementDao;
import smarthome.back.Model.ModelDatabase.RightElement;

import java.util.ArrayList;
import java.util.List;

public class SmartRights {
    public static List<RightElement> ListRights = new ArrayList<>();

    public static void GetListRightsFromDAO(){
        RightElementDao rightElementDao = new RightElementDao();
        ListRights = rightElementDao.getAllRight();
    }
}
