package smarthome.back.SmartObject;

import smarthome.back.Dao.UserDao;
import smarthome.back.Model.ModelFront.UserFront;
import smarthome.back.Model.ModelDatabase.User;


import java.util.ArrayList;
import java.util.List;

public class SmartUsers {
    public static List<UserFront> listUsersFront = new ArrayList<>();

    public static void GetListUsersFromDAO(){
        UserDao userdao = new UserDao();
        List<User> userList;
        List<UserFront> userFrontList = new ArrayList<>();
        userList = userdao.getAllUser();

        for (User user: userList) {
            UserFront userFront = new UserFront();
            userFront.setLogin(user.getLogin());
            userFront.setPassword(user.getPassword());
            userFrontList.add(userFront);
        }

        listUsersFront = userFrontList;
    }

    public static UserFront GetUserByLogin(String login){
        for (UserFront user:listUsersFront) {
            if (user.getLogin() == login){
                return user;
            }
        }
        return null;
    }

}
