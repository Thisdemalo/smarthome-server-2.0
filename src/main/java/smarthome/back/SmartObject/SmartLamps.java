package smarthome.back.SmartObject;

import smarthome.back.Dao.FrontReadyDao;
import smarthome.back.Model.ModelFront.Lamp;

import java.util.ArrayList;
import java.util.List;

public class SmartLamps {
    public static List<Lamp> ListLamps = new ArrayList<Lamp>();

    public static void CreateListLamp() {

        /* Récupération de toutes les lampes sans distinction de l'utilisateur */
        ListLamps = GetAllLamps();

    }

    /*
    Méthode surement obsolètes (revoire toutes les méthodes sur la sélection d'une lampe by user
     */
    public static List<Lamp> GetAllLampByUser(String login){
        FrontReadyDao frontReadyDao = new FrontReadyDao();
        return frontReadyDao.getAllLampByUser(login);
    }

    public static List<Lamp> GetAllLamps(){
        FrontReadyDao frontReadyDao = new FrontReadyDao();
        return frontReadyDao.getAllLamps();
    }


/*
    public static void listenToComponents(){

        try {
            InetAddress host = InetAddress.getLocalHost();
            Socket socket = new Socket("localhost", 8888);
            BufferedReader in = new BufferedReader (new InputStreamReader(socket.getInputStream ()));
            while (true)
            {
                String cominginText = "";

                cominginText = in.readLine ();
                System.out.println (cominginText);

            }

        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }*/
}
