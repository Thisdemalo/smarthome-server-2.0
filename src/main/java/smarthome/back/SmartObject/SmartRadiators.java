package smarthome.back.SmartObject;

import smarthome.back.Dao.FrontReadyDao;
import smarthome.back.Model.ModelFront.Lamp;
import smarthome.back.Model.ModelFront.Radiator;

import java.util.ArrayList;
import java.util.List;

public class SmartRadiators {
    public static List<Radiator> ListRadiators = new ArrayList<Radiator>();

    public static void CreateListRadiator() {
        ListRadiators = GetAllRadiators();

    }

    /*
    Méthode surement obsolètes (revoire toutes les méthodes sur la sélection d'une lampe by user
    */
    public static List<Radiator> GetAllRadiatorsByUser(String login){
        FrontReadyDao frontReadyDao = new FrontReadyDao();
        return frontReadyDao.getAllRadiators();
    }

    public static List<Radiator> GetAllRadiators(){
        FrontReadyDao frontReadyDao = new FrontReadyDao();
        return frontReadyDao.getAllRadiators();
    }

}
