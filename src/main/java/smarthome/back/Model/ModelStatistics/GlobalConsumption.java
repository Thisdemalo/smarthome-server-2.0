package smarthome.back.Model.ModelStatistics;

public class GlobalConsumption {

    private int hour;
    private int day;
    private int month;
    private int year;
    private float consumptionWattHours;

    public GlobalConsumption() {
    }

    /*
     * GlobalConsumptionHours
     */
    public GlobalConsumption(int hour, int day, int month, int year, float consumptionWattHours) {
        this.hour = hour;
        this.day = day;
        this.month = month;
        this.year = year;
        this.consumptionWattHours = consumptionWattHours;
    }

    /*
     * GlobalConsumptionDay
     */
    public GlobalConsumption(int day, int month, int year, float consumptionWattHours) {
        this.day = day;
        this.month = month;
        this.year = year;
        this.consumptionWattHours = consumptionWattHours;
    }

    /*
     * GlobalConsumptionMonth
     */
    public GlobalConsumption(int month, int year, float consumptionWattHours) {
        this.month = month;
        this.year = year;
        this.consumptionWattHours = consumptionWattHours;
    }

    /*
     * GlobalConsumptionYear
     */
    public GlobalConsumption(int year, float consumptionWattHours) {
        this.year = year;
        this.consumptionWattHours = consumptionWattHours;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public float getConsumptionWattHours() {
        return consumptionWattHours;
    }

    public void setConsumptionWattHours(float consumptionWattHours) {
        this.consumptionWattHours = consumptionWattHours;
    }
}
