package smarthome.back.Model.ModelStatistics;

public class ElementConsumption {

    private int hour;
    private int day;
    private int month;
    private int year;
    private String nameElement;
    private String roomElement;
    private int element;
    private float durationHour;
    private float consumptionWattHours;

    public ElementConsumption() {
    }

    /*
     * ElementConsumptionHours
     */
    public ElementConsumption(int hour, int day, int month, int year, String nameElement, String roomElement, int element, float durationHour, float consumptionWattHours) {
        this.hour = hour;
        this.day = day;
        this.month = month;
        this.year = year;
        this.nameElement = nameElement;
        this.roomElement = roomElement;
        this.element = element;
        this.durationHour = durationHour;
        this.consumptionWattHours = consumptionWattHours;
    }

    /*
     * ElementConsumptionDay
     */
    public ElementConsumption(int day, int month, int year, String nameElement, String roomElement, int element, float durationHour, float consumptionWattHours) {
        this.day = day;
        this.month = month;
        this.year = year;
        this.nameElement = nameElement;
        this.roomElement = roomElement;
        this.element = element;
        this.durationHour = durationHour;
        this.consumptionWattHours = consumptionWattHours;
    }

    /*
     * ElementConsumptionMonth
     */
    public ElementConsumption(int month, int year, String nameElement, String roomElement, int element, float durationHour, float consumptionWattHours) {
        this.month = month;
        this.year = year;
        this.nameElement = nameElement;
        this.roomElement = roomElement;
        this.element = element;
        this.durationHour = durationHour;
        this.consumptionWattHours = consumptionWattHours;
    }

    /*
     * ElementConsumptionYear
     */
    public ElementConsumption(int year, String nameElement, String roomElement, int element, float durationHour, float consumptionWattHours) {
        this.year = year;
        this.nameElement = nameElement;
        this.roomElement = roomElement;
        this.element = element;
        this.durationHour = durationHour;
        this.consumptionWattHours = consumptionWattHours;
    }

    @Override
    public String toString() {
        return "ElementConsumption{" +
                "hour=" + hour +
                ", day=" + day +
                ", month=" + month +
                ", year=" + year +
                ", nameElement='" + nameElement + '\'' +
                ", roomElement='" + roomElement + '\'' +
                ", element=" + element +
                ", durationHour=" + durationHour +
                ", consumptionWattHours=" + consumptionWattHours +
                '}';
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getNameElement() {
        return nameElement;
    }

    public void setNameElement(String nameElement) {
        this.nameElement = nameElement;
    }

    public String getRoomElement() {
        return roomElement;
    }

    public void setRoomElement(String roomElement) {
        this.roomElement = roomElement;
    }

    public int getElement() {
        return element;
    }

    public void setElement(int element) {
        this.element = element;
    }

    public float getDurationHour() {
        return durationHour;
    }

    public void setDurationHour(float durationHour) {
        this.durationHour = durationHour;
    }

    public float getConsumptionWattHours() {
        return consumptionWattHours;
    }

    public void setConsumptionWattHours(float consumptionWattHours) {
        this.consumptionWattHours = consumptionWattHours;
    }
}
