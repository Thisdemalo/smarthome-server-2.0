package smarthome.back.Model.ModelStatistics;

public class RoomConsumption {


    private int hour;
    private int day;
    private int month;
    private int year;
    private String nameRoom;
    private int idRoom;
    private float durationHour;
    private float consumptionWattHours;


    public RoomConsumption() {
    }

    /*
     * RoomConsumptionHours
     */
    public RoomConsumption(int hour, int day, int month, int year, String nameRoom, int idRoom, float durationHour, float consumptionWattHours) {
        this.hour = hour;
        this.day = day;
        this.month = month;
        this.year = year;
        this.nameRoom = nameRoom;
        this.idRoom = idRoom;
        this.durationHour = durationHour;
        this.consumptionWattHours = consumptionWattHours;
    }

    /*
     * RoomConsumptionDay
     */
    public RoomConsumption(int day, int month, int year, String nameRoom, int idRoom, float durationHour, float consumptionWattHours) {
        this.day = day;
        this.month = month;
        this.year = year;
        this.nameRoom = nameRoom;
        this.idRoom = idRoom;
        this.durationHour = durationHour;
        this.consumptionWattHours = consumptionWattHours;
    }

    /*
     * RoomConsumptionMonth
     */
    public RoomConsumption(int month, int year, String nameRoom, int idRoom, float durationHour, float consumptionWattHours) {
        this.month = month;
        this.year = year;
        this.nameRoom = nameRoom;
        this.idRoom = idRoom;
        this.durationHour = durationHour;
        this.consumptionWattHours = consumptionWattHours;
    }

    /*
     * RoomConsumptionYear
     */
    public RoomConsumption(int year, String nameRoom, int idRoom, float durationHour, float consumptionWattHours) {
        this.year = year;
        this.nameRoom = nameRoom;
        this.idRoom = idRoom;
        this.durationHour = durationHour;
        this.consumptionWattHours = consumptionWattHours;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getNameRoom() {
        return nameRoom;
    }

    public void setNameRoom(String nameRoom) {
        this.nameRoom = nameRoom;
    }

    public int getIdRoom() {
        return idRoom;
    }

    public void setIdRoom(int idRoom) {
        this.idRoom = idRoom;
    }

    public float getDurationHour() {
        return durationHour;
    }

    public void setDurationHour(float durationHour) {
        this.durationHour = durationHour;
    }

    public float getConsumptionWattHours() {
        return consumptionWattHours;
    }

    public void setConsumptionWattHours(float consumptionWattHours) {
        this.consumptionWattHours = consumptionWattHours;
    }
}
