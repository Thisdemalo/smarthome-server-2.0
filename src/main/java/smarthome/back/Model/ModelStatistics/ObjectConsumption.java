package smarthome.back.Model.ModelStatistics;

public class ObjectConsumption {

    private int hour;
    private int day;
    private int month;
    private int year;
    private String nameObject;
    private int idObject;
    private float durationHour;
    private float consumptionWattHours;

    public ObjectConsumption() {
    }

    /*
     * ObjectConsumptionHours
     */
    public ObjectConsumption(int hour, int day, int month, int year, String nameObject, int idObject, float durationHour, float consumptionWattHours) {
        this.hour = hour;
        this.day = day;
        this.month = month;
        this.year = year;
        this.nameObject = nameObject;
        this.idObject = idObject;
        this.durationHour = durationHour;
        this.consumptionWattHours = consumptionWattHours;
    }

    /*
     * ObjectConsumptionDay
     */
    public ObjectConsumption(int day, int month, int year, String nameObject, int idObject, float durationHour, float consumptionWattHours) {
        this.day = day;
        this.month = month;
        this.year = year;
        this.nameObject = nameObject;
        this.idObject = idObject;
        this.durationHour = durationHour;
        this.consumptionWattHours = consumptionWattHours;
    }

    /*
     * ObjectConsumptionMonth
     */
    public ObjectConsumption(int month, int year, String nameObject, int idObject, float durationHour, float consumptionWattHours) {
        this.month = month;
        this.year = year;
        this.nameObject = nameObject;
        this.idObject = idObject;
        this.durationHour = durationHour;
        this.consumptionWattHours = consumptionWattHours;
    }

    /*
     * ObjectConsumptionYear
     */
    public ObjectConsumption(int year, String nameObject, int idObject, float durationHour, float consumptionWattHours) {
        this.year = year;
        this.nameObject = nameObject;
        this.idObject = idObject;
        this.durationHour = durationHour;
        this.consumptionWattHours = consumptionWattHours;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getNameObject() {
        return nameObject;
    }

    public void setNameObject(String nameObject) {
        this.nameObject = nameObject;
    }

    public int getIdObject() {
        return idObject;
    }

    public void setIdObject(int idObject) {
        this.idObject = idObject;
    }

    public float getDurationHour() {
        return durationHour;
    }

    public void setDurationHour(float durationHour) {
        this.durationHour = durationHour;
    }

    public float getConsumptionWattHours() {
        return consumptionWattHours;
    }

    public void setConsumptionWattHours(float consumptionWattHours) {
        this.consumptionWattHours = consumptionWattHours;
    }
}
