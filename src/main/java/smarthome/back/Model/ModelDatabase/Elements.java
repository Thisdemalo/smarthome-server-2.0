package smarthome.back.Model.ModelDatabase;

public class Elements {

    private int id;
    private String name;
    private int objectType;
    private int room;
    private String description;
    private int consumptionHours;

    public Elements() {
    }

    public Elements(int id, String name, int objectType, int room, String description, int consumptionHours) {
        this.id = id;
        this.name = name;
        this.objectType = objectType;
        this.room = room;
        this.description = description;
        this.consumptionHours = consumptionHours;
    }

    public int getConsumptionHours() {
        return consumptionHours;
    }

    public void setConsumptionHours(int consumptionHours) {
        this.consumptionHours = consumptionHours;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getObjectType() {
        return objectType;
    }

    public void setObjectType(int objectType) {
        this.objectType = objectType;
    }

    public int getRoom() {
        return room;
    }

    public void setRoom(int room) {
        this.room = room;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Elements{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", objectType=" + objectType +
                ", room=" + room +
                ", description='" + description + '\'' +
                ", consumptionHours=" + consumptionHours +
                '}';
    }
}
