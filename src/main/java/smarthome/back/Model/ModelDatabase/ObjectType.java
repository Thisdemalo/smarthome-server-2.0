package smarthome.back.Model.ModelDatabase;

public class ObjectType {

    private int id;
    private String name;
    private String description;

    public ObjectType(){}

    public ObjectType(int id, String name, String description){
        this.description=description;
        this.name=name;
        this.id=id;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "ObjectType{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
