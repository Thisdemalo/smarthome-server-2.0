package smarthome.back.Model.ModelDatabase;

import java.util.Date;

public class History {
    private int id;
    private int element;
    private int action;
    private Date start_date;
    private String position;
    private Date end_date;

    public History() {
        this.id = -1;
        this.element = -1;
        this.action = -1;
        this.start_date = null;
        this.position = "";
        this.end_date = null;
    }

    public History(int id, int element, int action, Date start_date, String position, Date end_date) {
        this.id = id;
        this.element = element;
        this.action = action;
        this.start_date = start_date;
        this.position = position;
        this.end_date = end_date;
    }

    public Date getStart_date() {
        return start_date;
    }

    public void setStart_date(Date start_date) {
        this.start_date = start_date;
    }

    public Date getEnd_date() {
        return end_date;
    }

    public void setEnd_date(Date end_date) {
        this.end_date = end_date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getElement() {
        return element;
    }

    public void setElement(int element) {
        this.element = element;
    }

    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return "History{" +
                "id=" + id +
                ", element=" + element +
                ", action=" + action +
                ", start_date=" + start_date +
                ", position='" + position + '\'' +
                ", end_date=" + end_date +
                '}';
    }
}
