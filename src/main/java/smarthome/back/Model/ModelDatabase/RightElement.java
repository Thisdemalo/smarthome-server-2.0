package smarthome.back.Model.ModelDatabase;

public class RightElement {
    private int id;
    private int user;
    private int element;
    private String rightType;

    public RightElement(){}

    public RightElement(int id, int user, int element, String rightType){
        this.id = id;
        this.user = user;
        this.element = element;
        this.rightType = rightType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRightType() {
        return rightType;
    }

    public void setRightType(String rightType) {
        this.rightType = rightType;
    }

    public int getElement() {
        return element;
    }

    public void setElement(int element) {
        this.element = element;
    }

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }
}
