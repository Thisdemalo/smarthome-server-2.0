package smarthome.back.Model.ModelFront;

import smarthome.back.Model.ModelDatabase.ObjectType;

public class Radiator extends DomoticObject{

    private int id;
    private ObjectType objectType;
    private String name;
    private String piece;
    private int intensity;

    public Radiator() {
        super();
        this.intensity = -1;
    }

    public Radiator(int id, String name, String piece, int intensity) {
        this.id = id;
        this.name = name;
        this.piece = piece;
        this.intensity = intensity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ObjectType getObjectType() {
        return objectType;
    }

    public void setObjectType(ObjectType objectType) {
        this.objectType = objectType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPiece() {
        return piece;
    }

    public void setPiece(String piece) {
        this.piece = piece;
    }

    public int getIntensity() {
        return intensity;
    }

    public void setIntensity(int intensity) {
        this.intensity = intensity;
    }

    @Override
    public String toString() {
        return "Radiator{" +
                "id=" + id +
                ", objectType=" + objectType +
                ", name='" + name + '\'' +
                ", piece='" + piece + '\'' +
                ", intensity=" + intensity +
                '}';
    }
}
