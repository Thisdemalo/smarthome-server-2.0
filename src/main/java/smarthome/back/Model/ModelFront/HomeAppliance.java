package smarthome.back.Model.ModelFront;

public abstract class HomeAppliance extends DomoticObject {

    private int state;

    public HomeAppliance(){
        super();
        this.state=-1;
    }

    public HomeAppliance(int id, String name, String piece, int state){
        this.id=id;
        this.name=name;
        this.piece=piece;
        this.state=state;
    }


}
