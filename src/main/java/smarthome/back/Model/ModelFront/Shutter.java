package smarthome.back.Model.ModelFront;

public class Shutter extends DomoticObject {

    private int closing;

    public Shutter(){
        super();
        this.closing = 0;
    }

    public Shutter(int id, String name, String piece, int closing){
        this.id = id;
        this.name = name;
        this.piece = piece;
        this.closing = closing;
    }

}
