package smarthome.back.Model.ModelFront;

public class Program extends HomeAppliance {

    private String startDate;
    private String endDate;

    public Program(){
        super();
        this.startDate = "0";
        this.endDate = "0";
    }

    public Program(int state, String startDate, String endDate){
        state = -1;
        this.startDate = startDate;
        this.endDate = endDate;
    }

}
