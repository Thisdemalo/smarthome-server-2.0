package smarthome.back.Model.ModelFront;

import smarthome.back.Model.ModelDatabase.ObjectType;

public class Lamp extends DomoticObject {

    private int id;
    private ObjectType objectType;
    private String name;
    private String piece;
    private int intensity;
    private String color;

    public Lamp() {
        super();
        this.intensity = -1;
        this.color = null;
    }

    public Lamp(int id, String name, String piece, int intensity, String color) {
        this.id = id;
        this.name = name;
        this.piece = piece;
        this.intensity = intensity;
        this.color = color;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPiece() {
        return piece;
    }

    public void setPiece(String piece) {
        this.piece = piece;
    }

    public int getIntensity() {
        return intensity;
    }

    public void setIntensity(int intensity) {
        this.intensity = intensity;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Lamp{" +
                "id=" + id +
                ", objectType=" + objectType +
                ", name='" + name + '\'' +
                ", piece='" + piece + '\'' +
                ", intensity=" + intensity +
                ", color='" + color + '\'' +
                '}';
    }
}


