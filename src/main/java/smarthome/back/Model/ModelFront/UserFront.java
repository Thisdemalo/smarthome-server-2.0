package smarthome.back.Model.ModelFront;

public class UserFront {
    private String id;
    private String login;
    private String password;

    public UserFront() {
        super();
        this.id = null;
        this.login = null;
        this.password = null;
    }

    public UserFront(String id, String login, String password){
        this.id = id;
        this.login = login;
        this.password = password;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
