package smarthome.back.WebService;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import smarthome.back.Dao.ObjectTypeDao;
import smarthome.back.Model.ModelDatabase.ObjectType;

public class ObjectService {

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/getobjectType/{id}")
    public ObjectType getObjectType(@PathVariable("id") int id){
        ObjectTypeDao ob = new ObjectTypeDao();
        return ob.find(id);
    }
}
