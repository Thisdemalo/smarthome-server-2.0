package smarthome.back.WebService;

import org.springframework.web.bind.annotation.*;
import smarthome.back.Controller.LampController;
import smarthome.back.Model.ModelFront.Lamp;

import java.util.List;

@RestController
public class LampService {

    @CrossOrigin(origins = "*")
    @GetMapping("/getAllLamps")
    public List<Lamp> getAllLamps() {
        return LampController.GetListLamps();
    }

    @CrossOrigin(origins = "*")
    @GetMapping("/getAllLampsByUser/{login:.+}")
    public List<Lamp> getAllLampsByUser(@PathVariable("login") String login){
        return LampController.GetListLampsByUser(login);
    }

    @CrossOrigin(origins = "*")
    @PostMapping("/setLampByID")
    public Lamp SetLampByID(@RequestBody Lamp lamp) {
        return LampController.SetLampByID(lamp);
    }

    @CrossOrigin(origins = "*")
    @PostMapping("/setAllLamps")
    public List<Lamp> SetAllLamps(@RequestBody List<Lamp> lampList) {

        return LampController.SetAllLamps(lampList);
    }




    /*
    private List<Lamp> Ligths= new ArrayList<Lamp>();

    public LampService() throws InterruptedException {
        this.Ligths.add(new Lamp(1, "light 1", 0));
        this.Ligths.add(new Lamp(2, "light 2", 60));
        this.Ligths.add(new Lamp(3, "light 3", 20));
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping("/LampService/getAllLights")
    public List<Lamp> getAllLights(){
        return this.Ligths;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(value = "/LampService/setLigth", headers="Content-Type=application/json", method = RequestMethod.POST)
    @ResponseBody
    public String setLigth(HttpServletRequest request, HttpServletResponse response, ModelFront model) {
        String jsonString = request.getParameter("json");

       // boolean ret = false;
      //  for (Lamp modifiedLight: this.Ligths
      //       ) {
      //      if (modifiedLight.id == light.id)
      //      {
      //          modifiedLight.brightness = light.brightness;
      //          if (modifiedLight.brightness == light.brightness)
      //              ret = true;
      //      }
      //  }

        return jsonString;
    }
*/
}
