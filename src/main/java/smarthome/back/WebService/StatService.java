package smarthome.back.WebService;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import smarthome.back.Controller.StatistiqueController;
import smarthome.back.Model.ModelStatistics.ElementConsumption;
import smarthome.back.Model.ModelStatistics.GlobalConsumption;
import smarthome.back.Model.ModelStatistics.ObjectConsumption;
import smarthome.back.Model.ModelStatistics.RoomConsumption;

import java.util.List;

@RestController
public class StatService {


    @CrossOrigin(origins = "*")
    @GetMapping("/getElementConsumption/{nameView}")
    public List<ElementConsumption> getElementConsumption(@PathVariable("nameView") String nameView){
        return StatistiqueController.getElementConsumption(nameView);
    }



    @CrossOrigin(origins = "*")
    @GetMapping("/getGlobalConsumption/{nameView}")
    public List<GlobalConsumption> getGlobalConsumption(@PathVariable("nameView") String nameView){
        return StatistiqueController.getGlobalConsumption(nameView);
    }



    @CrossOrigin(origins = "*")
    @GetMapping("/getObjectConsumption/{nameView}")
    public List<ObjectConsumption> getObjectConsumption(@PathVariable("nameView") String nameView){
        return StatistiqueController.getObjectConsumption(nameView);
    }



    @CrossOrigin(origins = "*")
    @GetMapping("/getRoomConsumption/{nameView}")
    public List<RoomConsumption> getRoomConsumption(@PathVariable("nameView") String nameView){
        return StatistiqueController.getRoomConsumption(nameView);
    }

}
