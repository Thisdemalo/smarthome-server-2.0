

package smarthome.back.WebService;


import org.springframework.web.bind.annotation.*;
import smarthome.back.Controller.UserController;
import smarthome.back.Model.ModelFront.AuthentObject;

@RestController
public class DataService {

    @CrossOrigin("*")
    @PostMapping("/checkAuthentification")
    public Boolean CheckAuthentification(@RequestBody AuthentObject authent) {
        return UserController.CheckAuthentification(authent);
    }


}
