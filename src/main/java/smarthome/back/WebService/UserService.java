package smarthome.back.WebService;

import org.springframework.web.bind.annotation.*;
import smarthome.back.Controller.UserController;
import smarthome.back.Model.ModelFront.UserFront;

import java.util.List;

public class UserService {

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/setRight")
    public int setRight(@RequestBody UserFront userFront) {

        return UserController.SetUserRight(userFront);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/unsetRight")
    public int unsetRight(@RequestBody UserFront userFront) {

        return UserController.UnsetUserRight(userFront);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/createUser")
    public int createUser(@RequestBody UserFront userFront) {

        return UserController.UnsetUserRight(userFront);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/getAllUser")
    public List<UserFront> getAllUser(){
        return UserController.GetAllUser();
    }
}
