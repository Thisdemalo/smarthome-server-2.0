package smarthome.back.WebService;

import org.springframework.web.bind.annotation.*;
import smarthome.back.Controller.LampController;
import smarthome.back.Controller.RadiatorController;
import smarthome.back.Model.ModelFront.Lamp;
import smarthome.back.Model.ModelFront.Radiator;

import java.util.List;

@RestController
public class RadiatorService {

    @CrossOrigin(origins = "*")
    @GetMapping("/getAllRadiators")
    public List<Radiator> getAllRadiators() {
        return RadiatorController.GetListRadiators();
    }

    @CrossOrigin(origins = "*")
    @GetMapping("/getAllRadiatorsByUser/{login:.+}")
    public List<Lamp> getAllRadiatorsByUser(@PathVariable("login") String login){
        return LampController.GetListLampsByUser(login);
    }

    @CrossOrigin(origins = "*")
    @PostMapping("/setRadiatorsByID")
    public Lamp setRadiatorsByID(@RequestBody Lamp lamp) {
        return LampController.SetLampByID(lamp);
    }

    @CrossOrigin(origins = "*")
    @PostMapping("/setAllRadiators")
    public List<Lamp> setAllRadiators(@RequestBody List<Lamp> lampList) {

        return LampController.SetAllLamps(lampList);
    }

}
